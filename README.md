# payPackage
the weixinPay and alipay package
# for install
composer require first236108/payment

# support payment type
```php
class PayType
{

    const ALI_APP = AliApp::class;   //支付宝APP支付
    const ALI_H5 = AliH5::class; //支付宝H5支付
    const ALI_QRPAY = AliQrpay::class; //支付宝二维码支付
    const ALI_QUERY = AliQuery::class; //支付宝支付查询
    const ALI_REFUND = AliRefund::class; //支付宝退款
    const ALI_REFUNDQUERY = AliRefundQuery::class;   //支付宝退款查询
    const ALI_TRANS = AliTrans::class;   //支付宝转账
    const ALI_WEB = AliWeb::class;  //支付宝网页调起支付


    const WX_APP = WxApp::class; //微信APP支付
    const WX_Minipro = WxMinipro::class; //微信小程序支付
    const WX_H5 = WxH5::class; //微信H5支付
    const WX_JSPAY = WxJspay::class; //微信公众号支付
    const WX_QRPAY = WxQrpay::class; //微信二维码支付
    const WX_QUERY = WxQuery::class; //微信支付查询
    const WX_REFUND = WxRefund::class; //微信退款
    const WX_REFUNDQUERY = WxRefundQuery::class;  //微信退款查询

}
```

#pay example
```php
	use Payment\Pay;
	use Payment\PayType;
	
	$payConf	= array(
		'ali'=>[
				'app_id'				=>	'2017103109632000',
				'charset'				=>	'UTF-8',
				'sign_type'				=>	'RSA2',
				'gatewayUrl'			=>	'https://openapi.alipay.com/gateway.do',
				'merchant_private_key'	=>	'MIIEogIBAAKCAQEAsrq1a7XLpsqmkm5lA6whw0gCG...',
				'alipay_public_key'		=>	'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgAQE...',
				'return_url'			=>	'https://pay.web.cn/notify/',
				'notify_url'			=>	'https://pay.web.cn/return/',
		],
		'wx'=>[
				'appid'					=>	'wxd61e5ed1a5c7b8b5',
				'mchid'					=>	'1488194000',
				'key'					=>	'0598e9a56c45cd5243d27394cb21be3b',
				'sign_type'				=>	'MD5',
				'secret'				=>	'cdcb1b56897754fb0459ba962bd01bba',
				'notify_url'			=>	'https://pay.web.cn/notify/',
		],
	);
			
	$payData	=	array(
		'subject'		=>	"支付测试",
		'out_trade_no'	=>	"202182347283741234",		
		'amount'		=>	0.01,
		'param'			=>	"{'goods_id':1001}",
	);

	try {			
		Pay::run(PayType::ALI_H5, strtoupper(substr(PayType::ALI_H5, 0, 2))=='WX'?$payConf['wx']:$payConf['ali'], $payData);
	} catch (Exception $e) {				
		$result = array(
			'ret' => 9,
			'msg' => $e->getMessage(),
		);
	}
```

#notify example
```php
use Payment\Notify;

//微信支付回调
public function wxNotifyAction()
    {
        #Log::out(file_get_contents('php://input'), 'wxnotify');
        $payConf    = [
				'appid'					=>	'wxd61e5ed1a5c7b8b5',
				'mchid'					=>	'1488194000',
				'key'					=>	'0598e9a56c45cd5243d27394cb21be3b',
				'sign_type'				=>	'MD5',
				'secret'				=>	'cdcb1b56897754fb0459ba962bd01bba',
				'notify_url'			=>	'https://pay.web.cn/notify/',
		];
        Notify::run('NotifyWx', $payConf, ['ordersModel', 'notifyProcess']);
    }
```

#refund exapmle
```php
use Payment\Pay;
use Payment\PayType;

try {
    $payConf    = [
				'appid'					=>	'wxd61e5ed1a5c7b8b5',
				'mchid'					=>	'1488194000',
				'key'					=>	'0598e9a56c45cd5243d27394cb21be3b',
				'sign_type'				=>	'MD5',
				'secret'				=>	'cdcb1b56897754fb0459ba962bd01bba',
				'notify_url'			=>	'https://pay.web.cn/notify/',
	];
    $refundData = [
        'refund_no' => uniqid('ref'),
        'trade_no' => 'transaction_id',
        'total_fee' => 0.01,
        'refund_fee' => 0.01,
    ];
    $paidType = 'wxpay';
    $refund = '';
    if ($paidType == 'wxpay') {
        $config['sslcert_path'] = __DIR__ . '../cert/apiclient_cert.pem';
        $config['sslkey_path'] = __DIR__ . '../cert/apiclient_key.pem';
        $refund = PayType::WX_REFUND;
    } elseif ($paidType == 'alipay') {
        $refund = PayType::ALI_REFUND;
    }
    $result = Pay::run($refund, $payConf, $refundData);
} catch (\Throwable $t) {
    echo $t->getMessage();
}
```