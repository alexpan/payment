<?php
require_once __DIR__ . '/../../vendor/autoload.php';

use Payment\WxPay;
use Payment\AliPay;

class Index
{
    public function wxh5pay()
    {
        # h5支付，url自行跳转
        $wxConfig     = require_once(__DIR__ . '/../wxconfig.php');
        $out_trade_no = '20181208' . mt_rand(1000, 9999);//商户订单号
        $payData      = [
            'subject'         => '测试支付',
            'body'            => 'pay body',
            'out_trade_no'    => $out_trade_no,
            'total_fee'       => '0.01',// 单位为元 ,最小为0.01
            'timeout_express' => time() + 600,// 表示必须 600s 内付款
            'return_param'    => 'data',// 一定不要传入汉字，只能是 字母 数字组合
            'goods_type'      => '1',// 0—虚拟类商品，1—实物类商品
            'return_url'      => 'http://igccc.com',
            'notify_url'      => 'http://igccc.com/notify'
        ];
        try {
            $pay = new WxPay($wxConfig);
            $pay->h5Pay($payData);
        } catch (Exception $e) {
            echo $e->errorMessage();
            exit;
        }
    }

    public function alih5pay()
    {
        # h5支付，url自行跳转
        $aliConfig    = require_once(__DIR__ . '/../aliconfig.php');
        $out_trade_no = '20181208' . mt_rand(1000, 9999);//商户订单号
        $payData      = [
            'subject'         => '测试支付',
            'body'            => 'pay body',
            'out_trade_no'    => $out_trade_no,
            'total_fee'       => '0.01',// 单位为元 ,最小为0.01
            'timeout_express' => time() + 600,// 表示必须 600s 内付款
            'return_param'    => 'data',// 一定不要传入汉字，只能是 字母 数字组合
            'goods_type'      => '1',// 0—虚拟类商品，1—实物类商品
            'return_url'      => 'http://igccc.com',
            'notify_url'      => 'http://igccc.com/notify'
        ];
        try {
            $pay = new AliPay($aliConfig);
            $pay->h5Pay($payData);
        } catch (Exception $e) {
            echo $e->errorMessage();
            exit;
        }
    }
}