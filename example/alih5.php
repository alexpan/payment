<?php
include "../autoload.php";

use Payment\Pay;
use Payment\PayType;

#测试
try{
	$config = [
			'app_id'               => "",
			//商户私钥，您的原始格式RSA私钥
			'merchant_private_key' => "",
			//异步通知地址
			'notify_url'           => "",
			//同步跳转
			'return_url'           => "",
			//编码格式
			'charset'              => "UTF-8",
			//签名方式
			'sign_type'            => "RSA2",
			//支付宝网关
			'gatewayUrl'           => "https://openapi.alipay.com/gateway.do",
			//支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
			'alipay_public_key'    => "",
			//最大查询重试次数
			'MaxQueryRetry'        => "10",
			//查询间隔
			'QueryDuration'        => "3"
	];
	$payData      = [
            'subject'         => '测试支付',
            'body'            => 'pay body',
            'out_trade_no'    => uniqid('ord'),
            'total_fee'       => '0.01',// 单位为元 ,最小为0.01
            'timeout_express' => time() + 600,// 表示必须 600s 内付款
            'return_param'    => 'data',// 一定不要传入汉字，只能是 字母 数字组合
            'goods_type'      => '1',// 0—虚拟类商品，1—实物类商品
            'return_url'      => 'http://igccc.com',
            'notify_url'      => 'http://igccc.com/notify'
    ];
	Pay::run(PayType::ALI_H5, $config, $payData);
}catch(Exception $e){
	echo $e->getMessage();
}
