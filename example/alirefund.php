<?php
include "../autoload.php";

use Payment\Pay;
use Payment\PayType;

#测试, 单项退款
try {
    $config = [
        'app_id' => "",
        //商户私钥，您的原始格式RSA私钥
        'merchant_private_key' => "",
        //异步通知地址
        'notify_url' => "",
        //同步跳转
        'return_url' => "",
        //编码格式
        'charset' => "UTF-8",
        //签名方式
        'sign_type' => "RSA2",
        //支付宝网关
        'gatewayUrl' => "https://openapi.alipay.com/gateway.do",
        //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
        'alipay_public_key' => "",
        //最大查询重试次数
        'MaxQueryRetry' => "10",
        //查询间隔
        'QueryDuration' => "3"
    ];
    $refundData = [
        'refund_no' => uniqid('ref'),
        'trade_no' => 'transaction_id',
        'total_fee' => 0.01,
        'refund_fee' => 0.01,
    ];
    $paidType = 'alipay';
    $refund = '';
    if ($paidType == 'wxpay') {
        $config['sslcert_path'] = __DIR__ . '../cert/apiclient_cert.pem';
        $config['sslkey_path'] = __DIR__ . '../cert/apiclient_key.pem';
        $refund = PayType::WX_REFUND;
    } elseif ($paidType == 'alipay') {
        $refund = PayType::ALI_REFUND;
    }
    $result = Pay::run($refund, $config, $refundData);
} catch (\Throwable $t) {
    echo $t->getMessage();
}



#测试, 可用于混合批量退款
try {
    $config = [
        'app_id' => "",
        //商户私钥，您的原始格式RSA私钥
        'merchant_private_key' => "",
        //异步通知地址
        'notify_url' => "",
        //同步跳转
        'return_url' => "",
        //编码格式
        'charset' => "UTF-8",
        //签名方式
        'sign_type' => "RSA2",
        //支付宝网关
        'gatewayUrl' => "https://openapi.alipay.com/gateway.do",
        //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
        'alipay_public_key' => "",
        //最大查询重试次数
        'MaxQueryRetry' => "10",
        //查询间隔
        'QueryDuration' => "3"
    ];
    $refundData = [
        'refund_no' => uniqid('ref'),
        'trade_no' => 'transaction_id',
        'total_fee' => 0.01,
        'refund_fee' => 0.01,
    ];
    $paidType = 'alipay';
    if ($paidType == 'wxpay') {
        $config['sslcert_path'] = __DIR__ . '../cert/apiclient_cert.pem';
        $config['sslkey_path'] = __DIR__ . '../cert/apiclient_key.pem';
        $refundPay = new Payment\WxRefund;
    } elseif ($paidType == 'alipay') {
        $refundPay = new Payment\AliRefund;
    }
    $result = $refundPay->config($config)->payData($refundData)->handle();
} catch (\Throwable $t) {
    echo $t->getMessage();
}