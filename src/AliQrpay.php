<?php

namespace Payment;

use Payment\Payment;
use Payment\Ali\Service\AlipayTradeService;
use Payment\Ali\Buildermodel\AlipayTradePrecreateContentBuilder;

class AliQrpay extends Payment
{
    public function handle()
    {
        $builder = new AlipayTradePrecreateContentBuilder();
        $builder->setOutTradeNo($this->payData['out_trade_no']);
        $builder->setTotalAmount($this->payData['amount']);
        $builder->setTimeExpress("5m");
        $builder->setSubject($this->payData['subject']);
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $builder->setNotifyUrl($notify_url);
        $builder->setDisablePayChinnels('pcredit,pcreditpayInstallment,creditCard,creditCardExpress,creditCardCartoon');
        $qrPay  = new AlipayTradeService($this->config);
        $result = $qrPay->qrPay($builder);
        $res    = $result->getResponse();
        if ($result->getTradeStatus() !== 'SUCCESS') {
            return ['ret' => 1, 'msg' => $res];
        }

        return ['ret' => 0, 'data' => $res->qr_code];
    }
}
