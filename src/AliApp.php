<?php

namespace Payment;

use Payment\Payment;
use Payment\Ali\Service\AlipayTradeService;
use Payment\Ali\Aop\request\AlipayTradeAppPayRequest;

class AliAPP extends Payment
{
    public function handle()
    {
        $order      = [
            'subject'      => $this->payData['subject'],
            'out_trade_no' => $this->payData['out_trade_no'],
            'total_amount' => $this->payData['amount'],
            'product_code' => 'QUICK_MSECURITY_PAY'
        ];
        $builder    = new AlipayTradeAppPayRequest();
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $builder->setNotifyUrl($notify_url);
        $builder->setBizContent(json_encode($order));
        $service  = new AlipayTradeService($this->config);
        $response = $service->app($builder, $notify_url);
        return $response;
    }
}
