<?php

namespace Payment;

use Payment\Payment;
use Payment\Wx\lib\WxPayApi;
use Payment\Wx\lib\WxPayUnifiedOrder;

class WxH5 extends Payment
{
    public function handle()
    {
        $input = new WxPayUnifiedOrder($this->config['key']);
        $input->SetBody($this->payData['subject']);
        $input->SetOut_trade_no($this->payData['out_trade_no']);
        $input->SetTotal_fee(round($this->payData['amount'] * 100));
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $input->SetNotify_url($notify_url);
        $input->SetTrade_type("MWEB");
        if ($this->payData['goods_tag']) $input->SetGoods_tag($this->payData['goods_tag']);
        if ($this->payData['attach']) $input->SetAttach($this->payData['attach']);
        if ($this->payData['scene_info']){
			$input->SetScene_info($this->payData['scene_info']);
		}else{
			$input->SetScene_info('{"h5_info": {"type":"Wap","wap_url": "'. $this->get_current_domain() .'","wap_name": "在线支付"}}');
		}
        $pay    = new WxPayApi($this->config);
        $result = $pay->unifiedOrder($input);

        if ($result['return_code'] == 'FAIL') {
            return ['ret' => 1, 'msg' => $result['return_msg']];
        }
        if ($result['result_code'] == 'FAIL') {
            return ['ret' => 2, 'msg' => $result['err_code_des']];
        }
        $url = $result['mweb_url'] . '&redirect_url=' . urlencode($this->payData['return_url']);
        echo "<script>location.href='{$url}'</script>";
        exit;
    }
	public function get_current_domain()
	{
		$current_url='http://';
		if(isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on'){
			$current_url='https://';
		}
		if($_SERVER['SERVER_PORT']!='80'){
			$current_url.=$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
		}else{
			$current_url.=$_SERVER['SERVER_NAME'];
		}
		return $current_url;
	}
}
