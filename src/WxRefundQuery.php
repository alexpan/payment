<?php

namespace Payment;

use Payment\Payment;
use Payment\Wx\lib\WxPayApi;
use Payment\Wx\lib\WxPayRefundQuery;

class WxRefundQuery extends Payment
{
    public function handle()
    {
        $input = new WxPayRefundQuery($this->config['key']);
        if (isset($this->payData['out_trade_no'])) {
            $input->SetOut_trade_no($this->payData['out_trade_no']);
        } elseif ($this->payData['trade_no']) {
            $input->SetTransaction_id($this->payData['trade_no']);
        } elseif ($this->payData['out_refund_no']) {
            $input->SetOut_refund_no($this->payData['out_refund_no']);
        } elseif ($this->payData['refund_no']) {
            $input->SetRefund_id($this->payData['refund_no']);
        } else {
            return ['ret' => 1, 'msg' => '订单号与微信交易号不能同时为空'];
        }

        $tool   = new WxPayApi($this->config);
        $result = $tool->refundQuery($input);
        if ($result['return_code'] == 'FAIL')
            return ['ret' => 2, 'msg' => $result['return_msg']];
        if ($result['result_code'] == 'FAIL')
            return ['ret' => 3, 'msg' => $result['err_code_des']];

        unset(
            $result['appid'],
            $result['mch_id'],
            $result['nonce_str'],
            $result['result_code'],
            $result['return_code'],
            $result['return_msg'],
            $result['sign']
        );
        for ($i = 0; $i < 50; $i++) {
            if (array_key_exists('refund_id_' . $i, $result)) {
                $result['refund_fee_' . $i] /= 100;//单次退款金额
            } else {
                break;
            }
        }
        $result['cash_fee']   /= 100;//订单支付总金额
        $result['refund_fee'] /= 100;//累计退款金额
        return ['ret' => 0, 'data' => $result];
    }
}
