<?php

namespace Payment;

use Payment\Payment;
use Payment\Ali\Service\AlipayTradeService;
use Payment\Ali\Buildermodel\AlipayTradeWapPayContentBuilder;

class AliH5 extends Payment
{
    public function handle()
    {
        $builder = new AlipayTradeWapPayContentBuilder();
        $builder->setBody($this->payData['subject']);
        $builder->setSubject($this->payData['subject']);
        $builder->setOutTradeNo($this->payData['out_trade_no']);
        $builder->setTotalAmount($this->payData['amount']);
        //$builder->setTimeExpress($timeout_express);
        $pay        = new AlipayTradeService($this->config);
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $pay->wapPay($builder, $this->payData['return_url'], $notify_url);
        exit;
    }
}
