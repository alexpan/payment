<?php

namespace Payment;

use Payment\Payment;
use Payment\Wx\lib\wxAppPay;
use Payment\Wx\lib\WxPayUnifiedOrder;

class WxMinipro extends Payment
{
    public function handle()
    {
        $input = new WxPayUnifiedOrder($this->config['key']);
        $input->SetBody($this->payData['subject']);
        $input->SetOut_trade_no($this->payData['out_trade_no']);
        $input->SetTotal_fee(round($this->payData['amount'] * 100));
        $input->SetSpbill_create_ip($this->get_client_ip());
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $input->SetNotify_url($notify_url);
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($this->payData['openid']);
        $pay    = new wxAppPay($this->config);
        $result = $pay->unifiedOrder($input);

        if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS') {
            $data = [
                'ret'  => 0,
                'data' => $pay->getWxMiniproPayParams($result['prepay_id'])
            ];
        } else {
            $data = [
                'ret' => 9,
                'msg' => $result['err_code_des'] ?? $result['return_msg']
            ];
        }
        return $data;
    }

    private function get_client_ip()
    {
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        else if (isset($_SERVER["HTTP_CLIENT_IP"]))
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        else if (isset($_SERVER["REMOTE_ADDR"]))
            $ip = $_SERVER["REMOTE_ADDR"];
        else if (isset($_SERVER['SSH_CLIENT']))
            $ip = explode(' ', $_SERVER['SSH_CLIENT'])[0] ?? 'Error';//兼容swoft服务端
        else if (getenv("HTTP_X_FORWARDED_FOR"))
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        else if (getenv("HTTP_CLIENT_IP"))
            $ip = getenv("HTTP_CLIENT_IP");
        else if (getenv("REMOTE_ADDR"))
            $ip = getenv("REMOTE_ADDR");
        else
            $ip = "Unknown";
        return $ip;
    }
}
