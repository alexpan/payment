<?php

namespace Payment;

use Payment\Payment;
use Payment\Ali\Service\AlipayTradeService;
use Payment\Ali\Buildermodel\AlipayTradeFastpayRefundQueryContentBuilder;

class AliRefundQuery extends Payment
{
    public function handle()
    {
        $builder      = new AlipayTradeFastpayRefundQueryContentBuilder();
        $out_trade_no = $this->payData['out_trade_no'] ?? $this->payData['trade_no'];
        if (!$out_trade_no) {
            return ['ret' => 1, 'msg' => '订单号与支付宝交易号不能同时为空'];
        }
        $out_refund_no = $this->payData['out_refund_no'] ?? $this->payData['refund_no'];
        if (!$out_refund_no) {
            return ['ret' => 1, 'msg' => '退款单号与支付宝退款交易号不能同时为空'];
        }
        $builder->setOutTradeNo($out_trade_no);
        $builder->setOutRequestNo($out_refund_no);
        $aop    = new AlipayTradeService($this->config);
        $result = $aop->refundQuery($builder);
        if (!isset($result->alipay_trade_fastpay_refund_query_response->code))
            return ['ret' => 1, 'msg' => '查询失败'];
        $result = $result->alipay_trade_fastpay_refund_query_response;
        if ($result->code != '10000')
            return ['ret' => 2, 'msg' => $result->msg . $result->sub_msg];
        return ['ret' => 0, 'msg' => json_decode(json_encode($result), true)];
    }
}
