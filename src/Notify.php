<?php

namespace Payment;

class Notify
{
    protected static $notifyTypes = [
        'NotifyAli',
        'NotifyWx',
    ];
    protected static $instance;

    protected static function getInstance($notifyType)
    {
        if (is_null(self::$instance)) {
            $class            = "Payment\\" . $notifyType;
            static::$instance = new $class;
        }
        return static::$instance;
    }

    public static function run($notifyType, $config, callable|array $callback)
    {
        if (in_array($notifyType, self::$notifyTypes)) {
            self::getInstance($notifyType)->config($config)->handle($callback);
        } else {
            throw new \Exception('未知的业务类型');
        }
    }
}
