<?php

namespace Payment;

use Payment\Payment;
use Payment\Ali\Service\AlipayTradeService;
use Payment\Ali\Buildermodel\AlipayTradeRefundContentBuilder;

class AliRefund extends Payment
{
    public function handle()
    {
        $builder = new AlipayTradeRefundContentBuilder();
        if (isset($this->payData['out_trade_no'])) {
            $builder->setOutTradeNo($this->payData['out_trade_no']);
        }
        if (isset($this->payData['trade_no'])) {
            $builder->setTradeNo($this->payData['trade_no']);
        }

        if (!isset($this->payData['out_trade_no']) && !isset($this->payData['trade_no'])) {
            return ['ret' => 1, 'msg' => '订单号与微信交易号不能同时为空'];
        }
        $builder->setRefundAmount($this->payData['refund_fee']);
        $builder->setOutRequestNo($this->payData['refund_no']);
        $aop    = new AlipayTradeService($this->config);
        $result = $aop->Refund($builder);
        if (!isset($result->code))
            return ['ret' => 2, 'msg' => '退款申请失败'];
        if ($result->code != '10000')
            return ['ret' => 3, 'msg' => $result->msg . $result->sub_msg];
        return ['ret' => 0, 'data' => [
            'out_trade_no'  => $result->out_trade_no,         //商户订单号码
            'trade_no'      => $result->trade_no,             //商户订单号码
            'out_refund_no' => $this->payData['refund_no'],     //商户提交的退款单号
            'refund_id'     => $result->trade_no,             //微信退款单号
            'refund_fee'    => $result->refund_fee,           //退款金额
        ]];
    }
}
