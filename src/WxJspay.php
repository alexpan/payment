<?php

namespace Payment;

use Payment\Payment;
use Payment\Wx\lib\WxPayApi;
use Payment\Wx\lib\JsApiPay;
use Payment\Wx\lib\WxPayUnifiedOrder;

class WxJspay extends Payment
{
    public function handle()
    {
        $tools = new JsApiPay($this->config);
        $input = new WxPayUnifiedOrder($this->config['key']);
        $input->SetBody($this->payData['subject']);
        $input->SetOut_trade_no($this->payData['out_trade_no']);
        $input->SetTotal_fee(round($this->payData['amount'] * 100));
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $input->SetNotify_url($notify_url);
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($this->payData['openid']);
        if (isset($this->payData['goods_tag'])) $input->SetGoods_tag($this->payData['goods_tag']);
        if (isset($this->payData['attach'])) $input->SetAttach($this->payData['attach']);

		$pay   = new WxPayApi($this->config);
        $result = $pay->unifiedOrder($input);
        if ($result['result_code']=='FAIL' || $result['return_code'] == 'FAIL') {
            throw new \Exception($result['return_msg']);
        }
        return $tools->GetJsApiParameters($result);		
    }
}
