<?php

namespace Payment;

abstract class Payment
{
    public $config = [
        'curl_proxy_host' => '0.0.0.0',
        'curl_proxy_post' => 0,
        'charset'         => 'UTF-8',
        'sign_type'       => 'RSA2'
    ];

    public $payData = [];

    public function config(array $config)
    {
        $this->config = array_merge($this->config, $config);
        return $this;
    }

    public function payData(array $payData)
    {
        $this->payData = $payData;
        return $this;
    }

    public abstract function handle();
}





