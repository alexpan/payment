<?php

namespace Payment;

use Payment\Payment;
use Payment\Ali\Service\AlipayTradeService;
use Payment\Ali\Buildermodel\AlipayTradeFundTransToaccountContentBuilder;

class AliTrans extends Payment
{
    public function handle()
    {
        $type    = [0 => 'ALIPAY_USERID', 1 => 'ALIPAY_LOGONID'];
        $builder = new AlipayTradeFundTransToaccountContentBuilder();
        $builder->setOutBizNo($this->payData['out_biz_no']);
        $builder->setPayeeType($type[$this->payData['payee_type']]);
        $builder->setPayeeAccount($this->payData['payee_account']);
        $builder->setAmount($this->payData['amount']);
        if (isset($this->payData['payer_show_name'])) $builder->setPayerShowName($this->payData['payer_show_name']);
        if (isset($this->payData['payee_real_name'])) $builder->setPayeeRealName($this->payData['payee_real_name']);
        if (isset($this->payData['remark'])) $builder->setRemark($this->payData['remark']);
        $pay    = new AlipayTradeService($this->config);
        $result = $pay->transfer($builder);
        $res    = $result->alipay_fund_trans_toaccount_transfer_response;
        if ($res->code != 10000)
            return ['ret' => 1, 'msg' => $res->sub_msg];
        return ['ret' => 0, 'result' => json_decode(json_encode($res), true)];
    }
}
