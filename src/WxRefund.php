<?php

namespace Payment;

use Payment\Wx\lib\WxPayApi;
use Payment\Wx\lib\WxPayRefund;

class WxRefund extends Payment
{
    public function handle()
    {
        $input = new WxPayRefund($this->config['key']);
        if (isset($this->payData['out_trade_no'])) {
            $input->SetOut_trade_no($this->payData['out_trade_no']);
        }
        if (isset($this->payData['trade_no'])) {
            $input->SetTransaction_id($this->payData['trade_no']);
        }
        $input->SetTotal_fee(round($this->payData['total_fee'] * 100));
        $input->SetRefund_fee(round($this->payData['refund_fee'] * 100));
        $input->SetOut_refund_no($this->payData['refund_no']);
        $input->SetOp_user_id($this->config['mchid']);
        $tool   = new WxPayApi($this->config);
        $result = $tool->refund($input);

        if ($result['return_code'] == 'FAIL') {
            return ['ret' => 2, 'msg' => $result['return_msg']];
        }
        if ($result['result_code'] == 'FAIL') {
            return ['ret' => 3, 'msg' => $result['err_code_des']];
        }
        return ['ret' => 0, 'data' => [
            'out_trade_no'  => $result['out_trade_no'],        //商户订单号码
            'trade_no'      => $result['transaction_id'],      //商户订单号码
            'out_refund_no' => $result['out_refund_no'],       //商户提交的退款单号
            'refund_id'     => $result['refund_id'],           //微信退款单号
            'refund_fee'    => $result['refund_fee'] / 100,    //退款金额
        ]];
    }
}
