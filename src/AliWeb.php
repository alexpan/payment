<?php

namespace Payment;

use Payment\Payment;
use Payment\Ali\Service\AlipayTradeService;
use Payment\Ali\Buildermodel\AlipayTradePagePayContentBuilder;

class AliWeb extends Payment
{
    public function handle()
    {
        $builder = new AlipayTradePagePayContentBuilder();
        $builder->setBody($this->payData['subject']);
        $builder->setSubject($this->payData['subject']);
        $builder->setTotalAmount($this->payData['amount']);
        $builder->setOutTradeNo($this->payData['out_trade_no']);
        $pay        = new AlipayTradeService($this->config);
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $result = $pay->pagePay($builder, $this->payData['return_url'], $notify_url);
        exit();//跳转
    }
}
