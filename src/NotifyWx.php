<?php

namespace Payment;

use Payment\NotifyRoot;
use Payment\Wx\lib\WxPayResults;
use Payment\Wx\lib\WxPayException;

class NotifyWx extends NotifyRoot
{
    public function handle(callable|array $callback)
    {
        $xml = trim(file_get_contents('php://input'));
        //如果返回成功则验证签名
        try {
            $tool   = new WxPayResults($this->config['key']);
            $result = $tool->Init($xml, $this->config['key']);
        } catch (WxPayException $e) {
            exit($e->errorMessage());
        }
        if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS')
            $res = call_user_func($callback, 'WX', $result);

        if ($res === true) {
            $tool->SetData('return_code', 'SUCCESS');
            $tool->SetData('return_msg', 'OK');
            $xml = $tool->ToXml();
            exit($xml);
        }
        exit('FAIL');
    }
}
