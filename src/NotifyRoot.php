<?php

namespace Payment;

abstract class NotifyRoot
{
    public $config = [];
    public $notifyData = [];

    public function config($config)
    {
        $this->config = $config;
        return $this;
    }

    public abstract function handle(callable $callback);
}





