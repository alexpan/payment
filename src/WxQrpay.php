<?php

namespace Payment;

use Payment\Payment;
use Payment\Wx\lib\NativePay;
use Payment\Wx\lib\WxPayUnifiedOrder;

class WxQrpay extends Payment
{
    public function handle()
    {
        $input = new WxPayUnifiedOrder($this->config['key']);
        $input->SetBody($this->payData['subject']);
        $input->SetOut_trade_no($this->payData['out_trade_no']);
        $input->SetTotal_fee(round($this->payData['amount'] * 100));
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $input->SetNotify_url($notify_url);
        $input->SetTrade_type("NATIVE");
        $input->SetProduct_id("123456");
        if (isset($this->payData['goods_tag'])) $input->SetGoods_tag($this->payData['goods_tag']);
        if (isset($this->payData['attach'])) $input->SetAttach($this->payData['attach']);

        $tool   = new NativePay();
        $result = $tool->GetPayUrl($input, $this->config);

        if ($result['return_code'] == 'FAIL') {
            return ['ret' => 1, 'msg' => $result['return_msg']];
        }
        if ($result['result_code'] == 'FAIL') {
            return ['ret' => 2, 'msg' => $result['err_code_des']];
        }
        return ['ret' => 0, 'data' => $result["code_url"]];

    }
}
