<?php

namespace Payment;

use Payment\Wx\lib\WxPayApi;
use Payment\Wx\lib\WxPayOrderQuery;

class WxQuery extends Payment
{
    public function handle()
    {
        $input = new WxPayOrderQuery($this->config['key']);
        if ($this->payData['out_trade_no']) {
            $input->SetOut_trade_no($this->payData['out_trade_no']);
        } elseif ($this->payData['trade_no']) {
            $input->SetTransaction_id($this->payData['trade_no']);
        } else {
            return ['ret' => 1, 'msg' => '订单号与微信交易号不能同时为空'];
        }
        $tool   = new WxPayApi($this->config);
        $result = $tool->orderQuery($input);

        if ($result['return_code'] == 'FAIL') {
            return ['ret' => 2, 'msg' => $result['return_msg']];
        }
        if ($result['result_code'] == 'FAIL') {
            return ['ret' => 3, 'msg' => $result['err_code_des']];
        }
        return [
            'ret'  => 0,
            'data' => [
                'attach'           => $result['attach'],//支付时提交的附加数据
                'bank_type'        => $result['bank_type'],//银行类型
                'cash_fee'         => $result['cash_fee'] / 100,//支付金额
                'fee_type'         => $result['fee_type'],//支付币种
                'is_subscribe'     => $result['is_subscribe'],//是否关注公众号
                'openid'           => $result['openid'],//
                'out_trade_no'     => $result['out_trade_no'],//商户订单号
                'time_end'         => $result['time_end'],//支付完成时间戳
                'total_fee'        => $result['total_fee'] / 100,//订单总金额
                'trade_state'      => $result['trade_state'],//交易状态
                'trade_state_desc' => $result['trade_state_desc'],//交易状态描述
                'trade_type'       => $result['trade_type'],//交易类型
                'transaction_id'   => $result['transaction_id'],//交易类型
            ]];
    }
}
