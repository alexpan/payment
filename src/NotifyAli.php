<?php

namespace Payment;

use Payment\NotifyRoot;
use Payment\Ali\Service\AlipayTradeService;

class NotifyAli extends NotifyRoot
{
    public function handle(callable|array $callback)
    {
        if (empty($_POST)) {
            exit('回调内容为空');
        }
        $service = new AlipayTradeService($this->config);
        $result  = $service->check($_POST);
        if ($result) {
            if ($_POST['trade_status'] == 'TRADE_SUCCESS' || $_POST['trade_status'] == 'TRADE_FINISHED')
                $res = call_user_func($callback, 'ALI', $_POST);
            if (true === $res)
                exit('success');
        }
        exit('fail');
    }
}
