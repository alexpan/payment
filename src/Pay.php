<?php

namespace Payment;

class Pay
{
    protected static $instance;

    protected static function getInstance($payType)
    {
        if (is_null(self::$instance)) {
            static::$instance = new $payType;
        }
        return static::$instance;
    }

    public static function run(string $payType, array $config, array $payData)
    {
        return self::getInstance($payType)
            ->config($config)
            ->payData($payData)
            ->handle();
    }
}
