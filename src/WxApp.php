<?php

namespace Payment;

use Payment\Payment;
use Payment\Wx\lib\wxAppPay;
use Payment\Wx\lib\WxPayUnifiedOrder;

class WxApp extends Payment
{
    public function handle()
    {
        $input = new WxPayUnifiedOrder($this->config['key']);
        $input->SetBody($this->payData['subject']);
        $input->SetOut_trade_no($this->payData['out_trade_no']);
        $input->SetTotal_fee(round($this->payData['amount'] * 100));
        $input->SetSpbill_create_ip($this->getIp());
        $notify_url = $this->payData['notify_url'] ?? $this->config['notify_url'];
        $input->SetNotify_url($notify_url);
        $input->SetTrade_type("APP");
        $pay    = new wxAppPay($this->config);
        $result = $pay->unifiedOrder($input);
        if ($result['return_code'] == 'SUCCESS') {
            $data = $pay->getAppPayParams($result['prepay_id']);
        } else {
            if ($result['return_code'] == 'FAIL') {
                $data = ['ret' => 1, 'msg' => $result['return_msg']];
            } else {
                $data = ['ret' => 2, 'msg' => $result['err_code_des']];
            }
        }
        return $data;
    }

    private function getIp()
    {
        $ip = false;
        if (!empty($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ips = explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            if ($ip) {
                array_unshift($ips, $ip);
                $ip = FALSE;
            }
            for ($i = 0; $i < count($ips); $i++) {
                if (!preg_match("^(10|172\.16|192\.168)\.", $ips[$i])) {
                    $ip = $ips[$i];
                    break;
                }
            }
        }
        if (!$ip && isset($_SERVER['SSH_CLIENT'])) {
            $ip = explode(' ', $_SERVER['SSH_CLIENT'])[0] ?? 'Error';
        }
        return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
    }

}
