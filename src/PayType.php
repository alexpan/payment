<?php

namespace Payment;

class PayType
{

    const ALI_APP = AliApp::class;   //支付宝APP支付
    const ALI_H5 = AliH5::class; //支付宝H5支付
    const ALI_QRPAY = AliQrpay::class; //支付宝二维码支付
    const ALI_QUERY = AliQuery::class; //支付宝支付查询
    const ALI_REFUND = AliRefund::class; //支付宝退款
    const ALI_REFUNDQUERY = AliRefundQuery::class;   //支付宝退款查询
    const ALI_TRANS = AliTrans::class;   //支付宝转账
    const ALI_WEB = AliWeb::class;  //支付宝网页调起支付


    const WX_APP = WxApp::class; //微信APP支付
    const WX_Minipro = WxMinipro::class; //微信小程序支付
    const WX_H5 = WxH5::class; //微信H5支付
    const WX_JSPAY = WxJspay::class; //微信公众号支付
    const WX_QRPAY = WxQrpay::class; //微信二维码支付
    const WX_QUERY = WxQuery::class; //微信支付查询
    const WX_REFUND = WxRefund::class; //微信退款
    const WX_REFUNDQUERY = WxRefundQuery::class;  //微信退款查询

}
