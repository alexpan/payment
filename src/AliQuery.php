<?php

namespace Payment;

use Payment\Payment;
use Payment\Ali\Service\AlipayTradeService;
use Payment\Ali\Buildermodel\AlipayTradeQueryContentBuilder;

class AliQuery extends Payment
{
    public function handle()
    {
        $trade_no = $this->payData['trade_no'];
        $out_trade_no = $this->payData['out_trade_no'];
        $builder = new AlipayTradeQueryContentBuilder();
        if ($out_trade_no) {
            $builder->setOutTradeNo($out_trade_no);
        } elseif ($trade_no) {
            $builder->setTradeNo($trade_no);
        } else {
            return ['ret' => 1, 'msg' => '订单号与交易号不能同时为空'];
        }
        $aop    = new AlipayTradeService($this->config);
        $result = $aop->Query($builder);
        if (!isset($result->code))
            return ['ret' => 2, 'msg' => '查询失败'];
        if ($result->code != '10000')
            return ['ret' => 3, 'msg' => $result->sub_msg];
        return ['ret' => 0, 'msg' => json_decode(json_encode($result), true)];
    }
}
